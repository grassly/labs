using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Random = UnityEngine.Random;

public class MobEcsStarter : MonoBehaviour
{
    private EntityManager _entityManager;
    [SerializeField] private GameObject[] prefabs;
    [SerializeField] private int amount;

    private void Awake()
    {
        _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        GameObjectConversionSettings settings =
            GameObjectConversionSettings
                .FromWorld(World.DefaultGameObjectInjectionWorld, null);

        Entity[] prefabsEntities = new Entity[prefabs.Length];

        for (int i = 0; i < prefabs.Length; i++)
        {
            Entity prefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefabs[i], settings);
            prefabsEntities[i] = prefab;
        }

        for (int i = 0; i < amount; i++)
        {
            Entity currentToSpawn = prefabsEntities[Random.Range(0, prefabsEntities.Length)];
            Entity prefabInstance = _entityManager.Instantiate(currentToSpawn);
            
            float3 vector = new float3(Random.Range(-100, 100), 0, Random.Range(-100, 100));
            var position = transform.TransformPoint(vector);
            
            _entityManager.SetComponentData(prefabInstance, 
                new Translation {Value = position});
            _entityManager.SetComponentData(prefabInstance, 
                new Rotation {Value = new quaternion(0,0,0,0)});
        }
    }
}
