using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Jobs;

public class VerticalMovementController : MonoBehaviour
{
    [SerializeField] private GameObject lionPrefab;
    [SerializeField] private float speed;
    [SerializeField] private int lionAmount;
    [SerializeField] private Transform parentTransform;

    private Transform[] _objectsOnScene;

    private VerticalMovementJob _movementJob;
    private JobHandle _jobHandle;
    private TransformAccessArray transforms;
    void Start()
    {
        _objectsOnScene = new Transform[lionAmount];

        for (int i = 0; i < lionAmount; i++)
        {
            Vector3 spawnPoint = new Vector3(Random.Range(-100, 100), 100, Random.Range(-100, 100));
            GameObject lion = Instantiate(lionPrefab, spawnPoint, Quaternion.identity);
            _objectsOnScene[i] = lion.transform;
        }

        transforms = new TransformAccessArray(_objectsOnScene);
    }

    void Update()
    {
        _movementJob = new VerticalMovementJob(speed) { };
        _jobHandle = _movementJob.Schedule(transforms);
    }

    private void LateUpdate() => 
        _jobHandle.Complete();

    private void OnDisable() => 
        transforms.Dispose();
}

public struct VerticalMovementJob : IJobParallelForTransform
{
    private float _speed;
    public VerticalMovementJob(float speed) => 
        _speed = speed;

    public void Execute(int index, TransformAccess transform) => 
        transform.position += -Vector3.up * _speed;
}
