using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using Random = UnityEngine.Random;

public class ECSStarter : MonoBehaviour
{
    private EntityManager _entityManager;
    [SerializeField] private GameObject prefab;
    [SerializeField] private int amount;

    private void Awake()
    {
        _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        GameObjectConversionSettings settings = GameObjectConversionSettings
            .FromWorld(World.DefaultGameObjectInjectionWorld, null);
        Entity prefabEntity = GameObjectConversionUtility.ConvertGameObjectHierarchy(prefab, settings);
        
        for (int i = 0; i < amount; i++)
        {
            Entity prefabInstance = _entityManager.Instantiate(prefabEntity);
            float3 vector = new float3(Random.Range(-100, 100), 0, Random.Range(-100, 100));
            var position = transform.TransformPoint(vector);
            
            _entityManager.SetComponentData(prefabInstance, new Translation {Value = position});
            _entityManager.SetComponentData(prefabInstance, new Rotation {Value = new quaternion(0,0,0,0)});
        }
    }
}
