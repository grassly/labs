using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Jobs;
using Random = UnityEngine.Random;

public class MovementController : MonoBehaviour
{
    [SerializeField] private GameObject lionPrefab;
    [SerializeField] private float speed;
    [SerializeField] private int lionAmount;
    [SerializeField] private Transform parentTransform;

    private Transform[] _objectsOnScene;

    private MovementJob _movementJob;
    private JobHandle _jobHandle;
    private TransformAccessArray transforms;
    void Start()
    {
        _objectsOnScene = new Transform[lionAmount];

        for (int i = 0; i < lionAmount; i++)
        {
            Vector3 spawnPoint = new Vector3(Random.Range(-100, 100), 0, Random.Range(-100, 100));
            GameObject lion = Instantiate(lionPrefab, spawnPoint, Quaternion.identity);
            _objectsOnScene[i] = lion.transform;
        }

        transforms = new TransformAccessArray(_objectsOnScene);
    }

    void Update()
    {
        _movementJob = new MovementJob() { };
        _jobHandle = _movementJob.Schedule(transforms);
    }

    private void LateUpdate() => 
        _jobHandle.Complete();

    private void OnDisable() => 
        transforms.Dispose();
}