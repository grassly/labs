﻿using UnityEngine;
using UnityEngine.Jobs;

public struct MovementJob : IJobParallelForTransform
{
    public void Execute(int index, TransformAccess transform)
    {
        transform.position += Vector3.forward * 0.5f;
    }
}