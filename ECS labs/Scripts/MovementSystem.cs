using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

public class PlayerMovementSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        //List<JobHandle> handles = new List<JobHandle>();
        
        JobHandle playerJobHandler = Entities
            .WithName("PlayerMovementSystem")
            .ForEach((ref Translation translation, ref Rotation rotation, ref PlayerData playerData) =>
            {
                    translation.Value.z += 0.1f * playerData.speed;
            })
            .WithoutBurst()
            .Schedule(inputDeps);

        //handles.Add(playerJobHandler);
        //handles.Add(jumpingMobJobHandler);
        //handles.Add(dragMobJobHandler);
        
        //JobHandle allHandles = JobHandle.CombineDependencies(handles.ToNativeArray(Allocator.None));
        return playerJobHandler;
    }
}