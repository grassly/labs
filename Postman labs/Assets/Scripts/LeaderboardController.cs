using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Zenject;

public class LeaderboardController : MonoBehaviour
{
    [SerializeField] private Button getRequestButton;
    [SerializeField] private Button clearRequestButton;
    [SerializeField] private Button todayRequestButton;
    [SerializeField] private Button createUserButton;
    [Space(20)]
    [SerializeField] private LeaderboardClientConfig leaderboardClientConfig;
    [SerializeField] private Game coroutineRunner;
    [SerializeField] private LeaderboardView leaderboardView;
    private LeaderboardClient _leaderboardClient;

    [Inject]
    private void Construct(LeaderboardClient leaderboardClient)
    {
        _leaderboardClient = leaderboardClient;
        Debug.Log(_leaderboardClient);
    }

    private void Awake()
    {
        ApplyGetButton();
        ApplyClearButton();
        ApplyTodayButton();
        ApplyCreateUserButton();
    }

    private void ApplyGetButton()
    {
        getRequestButton.onClick.RemoveAllListeners();
        getRequestButton.onClick.AddListener(() =>
        {
            _leaderboardClient.SendGetAllUsersRequest(coroutineRunner, leaderboardClientConfig.GetAllUsers, 
                list =>
                {
                    leaderboardView.UpdateLeaderboardData(list);
                }
            );
        });
    }

    private void ApplyClearButton()
    {
        clearRequestButton.onClick.RemoveAllListeners();
        clearRequestButton.onClick.AddListener(() =>
        {
            _leaderboardClient.SendClearUsersRequest(coroutineRunner, leaderboardClientConfig.ClearUsersUrl, 
                () =>
                {
                    leaderboardView.PrintMessage("No users!");
                }
            );
        });
    }

    private void ApplyTodayButton()
    {
        todayRequestButton.onClick.RemoveAllListeners();
        todayRequestButton.onClick.AddListener(() =>
        {
            _leaderboardClient.SendGetTodayUsersRequest(coroutineRunner, leaderboardClientConfig.GetTodayUsersUrl,
                list =>
                {
                    leaderboardView.UpdateLeaderboardData(list);
                });
        });
    }

    private void ApplyCreateUserButton()
    {
        createUserButton.onClick.RemoveAllListeners();
        createUserButton.onClick.AddListener(() =>
        {
            _leaderboardClient.SendCreateUserRequest(coroutineRunner, () =>
            {
                Debug.Log("Completed!");
            });
        });
    }
}
