using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

public class LeaderboardClient
{
    public void SendGetAllUsersRequest(ICoroutineRunner coroutineRunner, string url, 
        Action<List<User>> onLoaded = null) => 
        coroutineRunner.StartCoroutine(GetAllUsersRequest(url, onLoaded));

    private IEnumerator GetAllUsersRequest(string url, Action<List<User>> onLoaded = null)
    {
        using UnityWebRequest request = UnityWebRequest.Get(url);
        yield return request.SendWebRequest();
        // if (request.isNetworkError)
        //     Debug.Log(request.error);
        //Debug.Log(request.downloadHandler.text);

        List<User> deserializedUsers =
            JsonConvert.DeserializeObject<List<User>>(request.downloadHandler.text);

        //foreach (User user in _userCollectionResponse.Users)
        //    Debug.Log(user.ToString());
        onLoaded?.Invoke(deserializedUsers);
    }

    public void SendClearUsersRequest(ICoroutineRunner coroutineRunner, string url, 
        Action onComplete = null) =>
        coroutineRunner.StartCoroutine(ClearUsersRequest(url, onComplete));

    private IEnumerator ClearUsersRequest(string url, Action onComplete = null)
    {
        using UnityWebRequest request = UnityWebRequest.Post(url, "");
        yield return request.SendWebRequest();
        onComplete?.Invoke();
    }

    public void SendGetTodayUsersRequest(ICoroutineRunner coroutineRunner, string url, 
        Action<List<User>> onFiltered = null) =>
        coroutineRunner.StartCoroutine(GetTodayUsersRequest(url, onFiltered));

    private IEnumerator GetTodayUsersRequest(string url, Action<List<User>> onFiltered = null)
    {
        using UnityWebRequest request = UnityWebRequest.Get(url);
        yield return request.SendWebRequest();
        List<User> deserializedTodayUsers =
            JsonConvert.DeserializeObject<List<User>>(request.downloadHandler.text);
        onFiltered?.Invoke(deserializedTodayUsers);
    }

    public void SendCreateUserRequest(ICoroutineRunner coroutineRunner, /*string url,*/ Action onPublished = null)
    {
        User user = new User()
        {
            Score = 170,
            PostStamp = DateTime.Now,
            Email = "flds"
        };
        coroutineRunner.StartCoroutine(CreateUserRequest("https://localhost:5001/user.create", user, onPublished));
    }

    private IEnumerator CreateUserRequest(string url, User user, Action onPublished = null)
    {
        WWWForm formData = new WWWForm();
        string userJson = JsonUtility.ToJson(user);
        string userJsonString = JsonConvert.SerializeObject(user);
        
        using UnityWebRequest request = UnityWebRequest.Post(url, formData); //проблемы с формированием запроса
        byte[] postBytes = Encoding.UTF8.GetBytes(userJsonString);
        UploadHandler uploadHandler = new UploadHandlerRaw(postBytes);
        request.uploadHandler = uploadHandler;
        request.SetRequestHeader("Content-Type", "application/json; charset=UTF-8");
        yield return request.SendWebRequest();

        onPublished?.Invoke();
    }
}