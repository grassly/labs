using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

public class LeaderboardView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI leaderboardTMP;

    private void Awake() =>
        ClearText();

    public void UpdateLeaderboardData(List<User> users)
    {
        ClearText();
        foreach (User user in users)
        {
            leaderboardTMP.text += user.ToString() + "\n";
        }
    }

    private void ClearText() =>
        leaderboardTMP.text = "";

    public void PrintMessage(string msg)
    {
        ClearText();
        leaderboardTMP.text = msg;
    }
}
