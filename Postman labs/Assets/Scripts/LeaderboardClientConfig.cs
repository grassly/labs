﻿using UnityEngine;

[CreateAssetMenu(menuName = "SO/new Leaderboard client config", fileName = "LeaderboardClient")]
public class LeaderboardClientConfig : ScriptableObject
{
    [SerializeField] private string getAllUsersUrl;
    public string GetAllUsers => getAllUsersUrl;

    [SerializeField] private string clearUsersUrl;
    public string ClearUsersUrl => clearUsersUrl;

    [SerializeField] private string getTodayUsersUrl;
    public string GetTodayUsersUrl => getTodayUsersUrl;
}