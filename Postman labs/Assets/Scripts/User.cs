﻿using System;

public class User
{
    public int Id { get; set; }
    public string Email { get; set; }
    public long Score { get; set; }
    public DateTime PostStamp { get; set; }

    public override string ToString() =>
        $"Id: {Id}; Email: {Email}; Score: {Score}; DateTime: {PostStamp}";
}